﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel
{
    class CurrentUser
    { 
        private CurrentUser() { }

        private static CurrentUser currentUser = new CurrentUser();

        public Client client;

        public static CurrentUser GetInstance()
        {
            return currentUser;
        }
    }
}
