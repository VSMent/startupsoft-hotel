﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel
{
    class Client
    {
        public static void createFirst()
        {
            Client cl = new Client("Admin", "istrator", "a", "a", 99999);
        }

        private Client(string firstName, string lastName, string login, string password, double money)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.login = login;
            this.password = password;
            this.money = Math.Round(money,2);
            Data.cList.Add(this);
        }

        public string firstName;
        public string lastName;
        public string login;
        public string password;
        public double money;
        public List<Hotel> bronedRooms = new List<Hotel>();

        public static Client Registration(string firstName, string lastName, string login, string password, double money)
        {   
            Client cl = new Client(firstName, lastName, login, password, money);
            return cl;               
        }

        public static CurrentUser LogIn(string login, string password)
        {
            CurrentUser currentUser = CurrentUser.GetInstance();
            foreach (Client client in Data.cList)
                if (login == client.login && password == client.password)
                {
                    currentUser.client = client;
                    return currentUser;
                }
            return null;
        }

        public void editData(int what, string str)
        {
            switch (what)
            {
                case 1:
                    {
                        this.firstName = str;
                        break;
                    }
                case 2:
                    {
                        this.lastName = str;
                        break;
                    }
                case 3:
                    {
                        this.login = str;
                        break;
                    }
                case 4:
                    {
                        this.password = str;
                        break;
                    }
                case 5:
                    { 
                        this.money = Math.Round(Convert.ToDouble(str),2);
                        break;
                    }
            }
        }
    }
}
