﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hotel
{
    public partial class Form3 : Form
    {
        Form1 loginForm;
        CurrentUser currentUser = CurrentUser.GetInstance();
        public Form3(Form1 frm1)
        {
            InitializeComponent();

            label1.Text = "First Name: " + currentUser.client.firstName;
            label2.Text = "Last Name: " + currentUser.client.lastName;
            label3.Text = "Login: " + currentUser.client.login;
            label4.Text = "Money: " + currentUser.client.money.ToString() + "$";

            listBox3.Hide();
            
            //foreach(Hotel hotel in Data.hList)
            //{
            //    if(hotel.roomCount > 0)
            //    {
            //        listBox1.Items.Add(hotel.name);
            //    }
            //}

            if(currentUser.client.bronedRooms != null)
            {
                listBox2.Items.Clear();

                foreach(Hotel hotel in currentUser.client.bronedRooms)
                {
                    listBox2.Items.Add(hotel.name);
                }
            }

            loginForm = frm1;
        }

        private void Form3_FormClosed(object sender, FormClosedEventArgs e)
        {
            loginForm.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form4 frm4 = new Form4(this);
            frm4.Show();
            this.Hide();
        }

        bool btn2_firstCick = true;
        private void button2_Click(object sender, EventArgs e)
        {
            if (btn2_firstCick)
            {
                btn6_firstCick = true;
                listBox3.Hide();
                listBox1.Show();
                listBox1.Items.Clear();
                foreach (Hotel hotel in Data.hList)
                {
                    if (hotel.roomCount > 0)
                    {
                        listBox1.Items.Add(hotel.name + " (" + hotel.price + "$)");
                    }
                }
                btn2_firstCick = false;
            }
            else
            {
                foreach (Hotel hotel in Data.hList)
                {
                    if (listBox1.SelectedItem != null)
                    {
                        string selectedString = "";
                        for(int i = 0; i < listBox1.SelectedItem.ToString().Length; i++)
                        {
                            if (listBox1.SelectedItem.ToString()[i] != ' ' && listBox1.SelectedItem.ToString()[i+1] != '(')
                            {
                                selectedString += listBox1.SelectedItem.ToString()[i];
                            }else
                            {
                                break;
                            }
                        }
                        if (selectedString == hotel.name)
                        {
                            if (hotel.roomCount > 0)
                            {
                                if(hotel.price > currentUser.client.money)
                                {
                                    MessageBox.Show("Not enough money.\nTo take room here you need " + hotel.price + "$");
                                }
                                if (hotel.roomCount == 1)
                                {
                                    listBox1.Items.Remove(hotel.name + " (" + hotel.price + "$)");
                                }
                                hotel.broneHotel(currentUser.client);
                                listBox2.Items.Add(hotel.name);
                                label4.Text = "Money: " + Math.Round(currentUser.client.money,2).ToString() + "$";
                                break;
                            }
                            else
                            {
                                listBox1.Items.Remove(hotel.name);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Select Hotel");
                        break;
                    }
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            btn2_firstCick = true;
            btn6_firstCick = true;
            listBox1.Items.Clear();
            foreach (Hotel hotel in Data.hList)
            {
                if (hotel.roomCount > 0)
                {
                    listBox1.Items.Add(hotel.name + " (" + hotel.price + "$)");
                }
            }

            listBox2.Items.Clear();
            if (currentUser.client.bronedRooms != null)
            {
                foreach (Hotel hotel in currentUser.client.bronedRooms)
                {
                    listBox2.Items.Add(hotel.name);
                }
            }

            label1.Text = "First Name: " + currentUser.client.firstName;
            label2.Text = "Last Name: " + currentUser.client.lastName;
            label3.Text = "Login: " + currentUser.client.login;
            label4.Text = "Money: " + currentUser.client.money.ToString() + "$";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form5 frm5 = new Form5(this);
            this.Hide();
            frm5.Show();
        }

        bool btn6_firstCick = true; 
        private void button6_Click(object sender, EventArgs e)
        {
            if (btn6_firstCick)
            {
                btn2_firstCick = true;
                listBox1.Hide();
                listBox3.Show();
                listBox3.Items.Clear();
                foreach (Hotel hotel in Data.hList)
                {
                    listBox3.Items.Add(hotel.name);
                }
                btn6_firstCick = false;
            }
            else
            {
                if (listBox3.SelectedItem != null)
                {
                    foreach (Hotel hotel in Data.hList)
                    {
                        if (listBox3.SelectedItem.ToString() == hotel.name)
                        {
                            if (hotel.owner == null)
                            {
                                MessageBox.Show("Hotel was successfully bought");
                                hotel.buyHotel(currentUser.client);
                                break;
                            }
                            else
                            {
                                MessageBox.Show("This hotel already has owner");
                                break;
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Select Hotel to buy");
                }
            }
        }
    }
}
