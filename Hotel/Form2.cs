﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hotel
{
    public partial class Form2 : Form
    {
        Form1 loginForm;
        public Form2(Form1 frm1)
        {
            InitializeComponent();
            loginForm = frm1;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(textBox1.Text) && !String.IsNullOrWhiteSpace(textBox2.Text) && !String.IsNullOrWhiteSpace(textBox3.Text) && !String.IsNullOrWhiteSpace(textBox4.Text) && !String.IsNullOrWhiteSpace(textBox5.Text))
            {
                string str = textBox5.Text;
                int count = 0;
                bool success = true;
                for(int i = 0; i < str.Length; i++)
                {
                    if (str[i] == ',' || str[i] == '.')
                    {
                        count++;
                        if(count > 1)
                        {
                            MessageBox.Show("To much comas");
                            success = false;
                            break;
                        }
                        if (str[i] == '.')
                        {
                            str = str.Replace('.', ',');
                        }
                    }
                }
                if (success)
                {
                    Client c1 = Client.Registration(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, Convert.ToDouble(str));
                    MessageBox.Show("Account was successfully created!");
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Can't be empty");
            }
        }
        
        private void button2_Click(object sender, EventArgs e)
        { 
            this.Close();
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            loginForm.Show();
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!Char.IsDigit(ch) && ch != 8 && ch != 44 && ch != 46) // 8 Backspace 44, 46 comma, dot
            {
                e.Handled = true;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (noTrippleLetter)
            {
                char ch = e.KeyChar;
                string str = textBox1.Text;
                if (str.Length > 1)
                {
                    if (str[str.Length - 1] == str[str.Length - 2] && str[str.Length - 1] == e.KeyChar)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        bool noTrippleLetter = false;
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                noTrippleLetter = true;
            }
            else
            {
                noTrippleLetter = false;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (noTrippleLetter)
            {
                char ch = e.KeyChar;
                string str = textBox3.Text;
                if (str.Length > 1)
                {
                    if (str[str.Length - 1] == str[str.Length - 2] && str[str.Length - 1] == e.KeyChar)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (noTrippleLetter)
            {
                char ch = e.KeyChar;
                string str = textBox2.Text;
                if (str.Length > 1)
                {
                    if (str[str.Length - 1] == str[str.Length - 2] && str[str.Length - 1] == e.KeyChar)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (noTrippleLetter)
            {
                char ch = e.KeyChar;
                string str = textBox4.Text;
                if (str.Length > 1)
                {
                    if (str[str.Length - 1] == str[str.Length - 2] && str[str.Length - 1] == e.KeyChar)
                    {
                        e.Handled = true;
                    }
                }
            }
        }
    }
}
