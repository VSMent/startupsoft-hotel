﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hotel
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 registrationForm = new Form2(this);
            this.Hide();
            registrationForm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(textBox1.Text) && !String.IsNullOrWhiteSpace(textBox2.Text))
            {
                CurrentUser currentUser = Client.LogIn(textBox1.Text, textBox2.Text);

                if (currentUser == null)
                {   
                    MessageBox.Show("Such client doesn't exists");
                }
                else
                {
                    Form3 userDataForm = new Form3(this);
                    userDataForm.Show();
                    this.Hide();
                }
            }else
            {
                MessageBox.Show("Can't be empty");
            }
        }

        bool noTrippleLetter = false;
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                noTrippleLetter = true;
            }else
            {
                noTrippleLetter = false;
            }
            
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (noTrippleLetter)
            {
                char ch = e.KeyChar;
                string str = textBox1.Text;
                if (str.Length > 1)
                {
                    if (str[str.Length - 1] == str[str.Length - 2] && str[str.Length - 1] == e.KeyChar)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (noTrippleLetter)
            {
                char ch = e.KeyChar;
                string str = textBox2.Text;
                if (str.Length > 1)
                {
                    if (str[str.Length - 1] == str[str.Length - 2] && str[str.Length - 1] == e.KeyChar)
                    {
                        e.Handled = true;
                    }
                }
            }
        }
    }
}
