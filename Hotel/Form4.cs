﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hotel
{
    public partial class Form4 : Form
    {
        Form3 userPageForm;
        CurrentUser currentUser = CurrentUser.GetInstance();
        public Form4(Form3 frm3)
        {
            InitializeComponent();

            textBox1.Text = currentUser.client.firstName;
            textBox2.Text = currentUser.client.lastName;
            textBox3.Text = currentUser.client.login;
            textBox4.Text = currentUser.client.password;
            textBox5.Text = currentUser.client.money.ToString();

            userPageForm = frm3;
        }

        private void Form4_FormClosed(object sender, FormClosedEventArgs e)
        {
            userPageForm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(textBox1.Text))
            {
                currentUser.client.editData(1, textBox1.Text);
                MessageBox.Show("Successfully changed");
            }
            else
            {
                MessageBox.Show("Can't be empty");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(textBox2.Text))
            {
                currentUser.client.editData(2, textBox2.Text);
                MessageBox.Show("Successfully changed");
            }
            else
            {
                MessageBox.Show("Can't be empty");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(textBox3.Text))
            {
                currentUser.client.editData(3, textBox3.Text);
                MessageBox.Show("Successfully changed");
            }
            else
            {
                MessageBox.Show("Can't be empty");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(textBox4.Text))
            {
                currentUser.client.editData(4, textBox4.Text);
                MessageBox.Show("Successfully changed");
            }
            else
            {
                MessageBox.Show("Can't be empty");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(textBox5.Text))
            {
                string str = textBox5.Text;
                int count = 0;
                bool success = true;
                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i] == ',' || str[i] == '.')
                    {
                        count++;
                        if (count > 1)
                        {
                            MessageBox.Show("To much comas");
                            success = false;
                            break;
                        }
                        if (str[i] == '.')
                        {
                            str = str.Replace('.', ',');
                        }
                    }
                }
                if (success)
                {
                    currentUser.client.editData(5, str);
                    MessageBox.Show("Successfully changed");
                }
            }
            else
            {
                MessageBox.Show("Can't be empty");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!Char.IsDigit(ch) && ch != 8 && ch != 44 && ch != 46) // 8 Backspace 44, 46 comma, dot
            {
                e.Handled = true;
            }
        }

        bool noTrippleLetter = false;
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                noTrippleLetter = true;
            }
            else
            {
                noTrippleLetter = false;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (noTrippleLetter)
            {
                char ch = e.KeyChar;
                string str = textBox1.Text;
                if (str.Length > 1)
                {
                    if (str[str.Length - 1] == str[str.Length - 2] && str[str.Length - 1] == e.KeyChar)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (noTrippleLetter)
            {
                char ch = e.KeyChar;
                string str = textBox2.Text;
                if (str.Length > 1)
                {
                    if (str[str.Length - 1] == str[str.Length - 2] && str[str.Length - 1] == e.KeyChar)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (noTrippleLetter)
            {
                char ch = e.KeyChar;
                string str = textBox3.Text;
                if (str.Length > 1)
                {
                    if (str[str.Length - 1] == str[str.Length - 2] && str[str.Length - 1] == e.KeyChar)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (noTrippleLetter)
            {
                char ch = e.KeyChar;
                string str = textBox4.Text;
                if (str.Length > 1)
                {
                    if (str[str.Length - 1] == str[str.Length - 2] && str[str.Length - 1] == e.KeyChar)
                    {
                        e.Handled = true;
                    }
                }
            }
        }
    }
}
