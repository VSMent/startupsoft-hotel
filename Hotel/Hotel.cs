﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel
{
    class Hotel
    {
        public static void createSet()
        { 
            Hotel h1 = new Hotel("Global", "Ternopil, Ruska 41", 100, 30.85);
            Hotel h2 = new Hotel("Local", "Ternopil, Morozenka 2a", 80, 40.80);
            Hotel h3 = new Hotel("Africa", "Ternopil, Tarnavskogo 51", 49, 50.01);
            Hotel h4 = new Hotel("Home", "Ternopil, 15 - Kvitnya 45", 24, 78.90);
            Hotel h5 = new Hotel("Olenka", "Ternopil, Konovaltsa 28", 2, 99.83);
        }

        public string name;
        public string adress;
        public int roomCount;
        public double price;
        public Client owner;

        public Hotel() { }

        public Hotel(string name, string adress, int roomCount, double price)
        {
            this.name = name;
            this.adress = adress;
            this.roomCount = roomCount;
            this.price = Math.Round(price,2);

            Data.hList.Add(this);
        }
        public Hotel(string name, string adress, int roomCount, double price, Client owner)
        {
            this.name = name;
            this.adress = adress;
            this.roomCount = roomCount;
            this.price = price;
            this.owner = owner;

            Data.hList.Add(this);
        }

        public Hotel searchHotel(string searchWord)
        {
            foreach (Hotel hot in Data.hList)
            {
                if (hot.name == searchWord || hot.adress == searchWord)
                {
                    return hot;
                }
            }
            return null;
        }

        public Hotel searchHotel(double searchPrice)
        {
            foreach (Hotel hot in Data.hList)
            {
                if (hot.price == searchPrice)
                {
                    return hot;
                }
            }
            return null;
        }

        public void broneHotel(Client cl)
        {
            if (cl.money >= this.price)
            {
                cl.money -= this.price;
                cl.bronedRooms.Add(this);
                this.roomCount--;
            }
        }

        public void buyHotel(Client cl)
        {
            if (this.owner == null)
            {
                this.owner = cl;
            }
        }
    }
}
