﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hotel
{
    public partial class Form5 : Form
    {
        Form3 userPageForm;
        public Form5(Form3 frm3)
        {
            InitializeComponent();
            userPageForm = frm3;
        }

        private void Form5_FormClosed(object sender, FormClosedEventArgs e)
        {
            userPageForm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(!String.IsNullOrWhiteSpace(textBox1.Text) && !String.IsNullOrWhiteSpace(textBox2.Text) && !String.IsNullOrWhiteSpace(textBox3.Text) && !String.IsNullOrWhiteSpace(textBox4.Text))
            {
                string str = textBox4.Text;
                int count = 0;
                bool success = true;
                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i] == ',' || str[i] == '.')
                    {
                        count++;
                        if (count > 1)
                        {
                            MessageBox.Show("To much comas");
                            success = false;
                            break;
                        }
                        if (str[i] == '.')
                        {
                            str = str.Replace('.', ',');
                        }
                    }
                }
                if (success)
                {
                    if (String.IsNullOrWhiteSpace(textBox5.Text))
                    {
                        Hotel hot = new Hotel(textBox1.Text, textBox2.Text, Convert.ToInt32(textBox3.Text), Convert.ToDouble(str));
                        MessageBox.Show("Hotel " + textBox1.Text + " was created without owner");
                        this.Close();
                    }
                    else
                    {
                        foreach (Client client in Data.cList)
                        {
                            if (textBox5.Text == client.firstName)
                            {
                                Hotel hot = new Hotel(textBox1.Text, textBox2.Text, Convert.ToInt32(textBox3.Text), Convert.ToDouble(str), client);
                                MessageBox.Show("Hotel " + textBox1.Text + " was created with owner: " + client.firstName + ".");
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("That user doesn't exists");
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Can`t be empty");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!Char.IsDigit(ch) && ch != 8 && ch != 44 && ch != 46) // 8 Backspace 44, 46 comma, dot
            {
                e.Handled = true;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!Char.IsDigit(ch) && ch != 8) // 8 Backspace 44, 46 comma, dot
            {
                e.Handled = true;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (noTrippleLetter)
            {
                char ch = e.KeyChar;
                string str = textBox1.Text;
                if (str.Length > 1)
                {
                    if (str[str.Length - 1] == str[str.Length - 2] && str[str.Length - 1] == e.KeyChar)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (noTrippleLetter)
            {
                char ch = e.KeyChar;
                string str = textBox2.Text;
                if (str.Length > 1)
                {
                    if (str[str.Length - 1] == str[str.Length - 2] && str[str.Length - 1] == e.KeyChar)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (noTrippleLetter)
            {
                char ch = e.KeyChar;
                string str = textBox5.Text;
                if (str.Length > 1)
                {
                    if (str[str.Length - 1] == str[str.Length - 2] && str[str.Length - 1] == e.KeyChar)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        bool noTrippleLetter = false;
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                noTrippleLetter = true;
            }
            else
            {
                noTrippleLetter = false;
            }
        }
    }
}
